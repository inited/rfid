package cz.roiinternational.rfid;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rscja.deviceapi.RFIDWithUHFA8;
import com.rscja.deviceapi.entity.UHFTAGInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "RFID";
    private static int POWER_MIN = 0;
    private static int POWER_MAX = 30;

    MainActivity mainActivity;
    ProgressDialog mDialog;

    EditText txtHostname;
    EditText txtPort;
    EditText txtUsername;
    EditText txtPassword;
    EditText txtDatabase;
    TextView tvStatus;
    CheckBox cbStartBoot;
    TextView tvPower;
    SeekBar sbPower;

    private boolean loopFlag = false;
    public RFIDWithUHFA8 mReader;

    private DBService dbService;

    private HashMap<String, Long> lastSeenMap;
    private SimpleDateFormat sdf;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = this;
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtHostname = findViewById(R.id.hostname);
        txtPort     = findViewById(R.id.port);
        txtUsername = findViewById(R.id.username);
        txtPassword = findViewById(R.id.password);
        txtDatabase = findViewById(R.id.database);
        cbStartBoot = findViewById(R.id.startBoot);
        tvPower     = findViewById(R.id.power);
        sbPower     = findViewById(R.id.sbPower);

        SharedPreferences prefs = getSharedPreferences("rfid_db", Context.MODE_PRIVATE);
        txtHostname.setText(prefs.getString("dbHostname", "172.24.0.1"));
        txtPort.setText(prefs.getString("dbPort", "3306"));
        txtUsername.setText(prefs.getString("dbUsername", "rfid"));
        txtPassword.setText(prefs.getString("dbPassword", "rfid"));
        txtDatabase.setText(prefs.getString("dbDatabase", "rfid"));
        cbStartBoot.setChecked(prefs.getBoolean("startBoot", false));

        int powerValue = prefs.getInt("power", (POWER_MAX + POWER_MIN) / 2);
        if (powerValue > POWER_MAX) {
            powerValue = POWER_MAX;
        }
        tvPower.setText("" + powerValue);
        sbPower.setProgress(powerValue);


        Button button1 = findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doConnectDB();
            }
        });

        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopScan();
            }
        });

        sbPower.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                tvPower.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        cbStartBoot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences prefs = getSharedPreferences("rfid_db", Context.MODE_PRIVATE);
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean("startBoot", isChecked);
                edit.apply();
            }
        });

        tvStatus = findViewById(R.id.status);
        tvStatus.setText("Ready");

        lastSeenMap = new HashMap<>();
        sdf = new SimpleDateFormat("HH:mm:ss");


        Intent serviceIntent = new Intent(this, BackgroundService.class);
        stopService(serviceIntent);

        // start after boot
        Intent intent = getIntent();
        if ("yes".equals(intent.getStringExtra("startBoot"))) {
            doConnectDB();
        }
    }

    private void doConnectDB() {

        String hostname = txtHostname.getText().toString();
        String port = txtPort.getText().toString();
        String username = txtUsername.getText().toString();
        String password = txtPassword.getText().toString();
        String database = txtDatabase.getText().toString();

        SharedPreferences prefs = getSharedPreferences("rfid_db", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString("dbHostname", hostname);
        edit.putString("dbPort", port);
        edit.putString("dbUsername", username);
        edit.putString("dbPassword", password);
        edit.putString("dbDatabase", database);
        edit.putInt("power", sbPower.getProgress());
        edit.apply();

        showProgressIndicator();

        if (dbService != null) {
            dbService.close();
        }
        dbService = new DBService(hostname, port, username, password, database);

        new ConnectAsyncTask().execute();
    }

    public void initUHF() {

        showProgressIndicator();
        try {
            mReader = RFIDWithUHFA8.getInstance();
            mReader.free();
            hideProgressIndicator();
        } catch (Throwable ex) {
            hideProgressIndicator();
            showAlert(ex.getMessage());
            return;
        }

        if (mReader != null) {
            showProgressIndicator();
            new InitUHFAsyncTask().execute();
        }
    }

    private void startScan() {

        loopFlag = true;

        // beware, operation order matters
        int[] antennas = new int[] { 1, 1, 1, 1, 1, 1, 1, 1 };
        mReader.setANT(antennas);
        mReader.setPower(sbPower.getProgress());
        mReader.startInventoryTag();
        new TagReadThread().start();
        status("Scanner running ...");
    }

    private void stopScan() {
        if (loopFlag) {
            loopFlag = false;
        }

        if (mReader != null) {
            mReader.stopInventory();
            mReader.free();
        }

        if (dbService != null) {
            dbService.close();
        }

        status("Ready");
    }

    private void showProgressIndicator() {
        mDialog = new ProgressDialog(mainActivity);
        mDialog.setMessage("Please wait...");
        mDialog.setCancelable(false);
        mDialog.show();
    }

    private void hideProgressIndicator() {
        if ((mDialog != null) && (mDialog.isShowing())) {
            mDialog.hide();
        }
    }

    private void showAlert(String msg) {
        AlertDialog alert = new AlertDialog.Builder(mainActivity).create();
        alert.setTitle("Error");
        alert.setMessage(msg);
        alert.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // finish();
            }
        });

        alert.show();
    }

    private void status(String msg) {
        tvStatus.setText(msg);
    }


    // pro volani a praci s DBService pouzivam private classes,
    // protoze ty maji pristup do UI

    private class ConnectAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                dbService.connect();
                return "OK";
            } catch (Exception e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            hideProgressIndicator();
            if ("OK".equals(result)) {
                status("DB connected");
                initUHF();
            } else {
                showAlert(result);
                status(result);
            }
        }
    }

    private class InsertAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            try {
                dbService.insertData(strings[0], strings[1], strings[2]);
                return "OK";
            } catch (Exception e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            hideProgressIndicator();
            if (!"OK".equals(result)) {
                // neukazovat alert, zahlti to obrazovku: showAlert(result);
                status(result);
            }
        }
    }

    public class InitUHFAsyncTask extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            return mReader.init();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            hideProgressIndicator();

            if (!result) {
                showAlert("UHF init failed");
                status("UHF init failed");
            } else {
                status("UHF inited");
                startScan();
            }
        }
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            synchronized (lastSeenMap) {

                UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                String epc = info.getEPC();

                // omezeni, abych jeden cip nezapsal 1000x za vterinu
                Long now = System.currentTimeMillis();
                if (lastSeenMap.containsKey(epc)) {
                    Long lastTime = lastSeenMap.get(epc);
                    if ((now - lastTime) <= 1000) {
                        return;
                    }
                }

                // nejprve vypisu info o scanu. Databaze pak muze jeste vypsat info o chybe
                status(sdf.format(new Date()) + " Ant: " + info.getAnt() + " Tag:" + epc);

                new InsertAsyncTask().execute(info.getEPC(), info.getAnt(), info.getRssi());
                lastSeenMap.put(epc,now);
            }
        }
    };


    class TagReadThread extends Thread {
        public void run() {
            UHFTAGInfo uhftagInfo;
            Message msg;
            while (loopFlag) {
                uhftagInfo = mReader.readTagFromBuffer();
                if (uhftagInfo != null) {
                    msg = handler.obtainMessage();
                    msg.obj = uhftagInfo;
                    handler.sendMessage(msg);
                }
            }
        }
    }
}
