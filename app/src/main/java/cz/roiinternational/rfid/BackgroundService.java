package cz.roiinternational.rfid;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import java.lang.ref.WeakReference;

public class BackgroundService extends Service {

    public static final String TAG = "BackgroundService";
    private Delayer delayTask = null;

    private final static String NOTIFICATION_TEXT = "I'm running. Click me.";
    private final static String NOTIFICATION_TICKER = "my ticker";
    private static final String NOTIFICATION_CHANNEL_ID_SERVICE = "com.mypackage.service";
    private static final String NOTIFICATION_CHANNEL_ID_INFO = "com.mypackage.download_info";
    private final static int ONGOING_NOTIFICATION_ID = 1;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        delayTask = new Delayer(this, getBaseContext());
        delayTask.execute("start");

        // setup notifications
        // it is required for Android 6+
        // otherwise the app is terminated after 10s
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_SERVICE, "App Service", NotificationManager.IMPORTANCE_DEFAULT));
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_INFO, "Download Info", NotificationManager.IMPORTANCE_DEFAULT));
        }

        // intent for start the app by click on notification
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Notification.Builder builder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID_INFO)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(NOTIFICATION_TEXT)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentIntent(pendingIntent)
                    .setTicker(NOTIFICATION_TICKER)
                    .setAutoCancel(true);

            Notification notification = builder.build();
            startForeground(ONGOING_NOTIFICATION_ID, notification);

        } else {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID_INFO)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(NOTIFICATION_TEXT)
                    .setSmallIcon(R.mipmap.ic_launcher)  // be sure to use bitmap icon, not vector
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setTicker(NOTIFICATION_TICKER)
                    .setAutoCancel(true);

            Notification notification = builder.build();
            startForeground(ONGOING_NOTIFICATION_ID, notification);
        }

    }

    @Override
    public void onDestroy() {
        if (delayTask != null) {
            delayTask.cancel(true);
        }
    }

    public void onFinish() {
        delayTask = null;
    }

    private static class Delayer extends AsyncTask<String, Integer, Integer> {

        //private BackgroundService service;
        private final WeakReference<BackgroundService> service;
        private final WeakReference<Context> ctx;

        public Delayer(BackgroundService service, Context ctx) {
            this.service = new WeakReference<>(service);
            this.ctx = new WeakReference<>(ctx);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i(TAG, "async task started");
        }

        @Override
        protected Integer doInBackground(String... params) {
            for(int i=0;i<12;i++) {
                int waittime = 60 - 5*i;
                publishProgress(waittime);
                try {
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    return 0;
                }
            }

            Intent intents = new Intent(ctx.get(), MainActivity.class);
            intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intents.putExtra("startBoot", "yes");
            ctx.get().startActivity(intents);
            return 1;
        }

        @Override
        protected void onPostExecute(Integer returnCode) {
            super.onPostExecute(returnCode);
            service.get().onFinish();
            Log.i(TAG, "async task finished");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Integer i = values[0];
            Toast.makeText(ctx.get(), "RFID start in " + i + "s", Toast.LENGTH_LONG).show();
        }

    }


}
