package cz.roiinternational.rfid;

import android.util.Log;

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DBService {

    private String hostname;
    private String port;
    private String username;
    private String password;
    private String database;

    Connection connection;



    public DBService(String hostname, String port, String username, String password, String database) {
        this.hostname = hostname;
        this.port = port;
        this.username = username;
        this.password = password;
        this.database = database;
    }

    public void connect() throws Exception {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            StringBuilder sb = new StringBuilder();
            sb.append("jdbc:mysql://");
            sb.append(hostname);
            sb.append(":");
            sb.append(port);
            sb.append("/");
            sb.append(database);

            String jdbcURL = sb.toString();
            Log.d("RFID", "JDBC URL:" + jdbcURL);

            connection = DriverManager.getConnection(jdbcURL, username, password);

        } catch (ClassNotFoundException e) {
            Log.e("RFID", "Chyba pripojeni", e);
            throw e;
        } catch (SQLException e) {
            Log.e("RFID", "Chyba pripojeni", e);

            String errorMessage = e.getMessage();
            if (e instanceof CommunicationsException) {
                // informace o problemech pripojeni jsou ve vnorene vyjimce
                Exception e2 = (Exception)e.getCause();
                throw e2;
            }
            throw e;
        }
    }

    public void insertData(String tagName, String antenna, String rssi) throws Exception {

        if ((connection == null) || (connection.isClosed())) {
            connect();
        }

        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO tags (tag, antenna, rssi) VALUES (?,?,?)");
        preparedStatement.setString(1, tagName);
        preparedStatement.setString(2, antenna);
        preparedStatement.setString(3, rssi);

        int rows = preparedStatement.executeUpdate();
        Log.i("RFID", "Ulozeno do db:" + rows);
    }

    public void close() {
        try {
            if ((connection == null) || (connection.isClosed())) {
                return;
            }
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
