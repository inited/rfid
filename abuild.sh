#!/bin/sh
VERSION=`grep versionName app/build.gradle | cut -d'"' -f2 | sed "s/\\./_/g"`
PROJNAME=rfid

./gradlew assembleDebug
mv app/build/outputs/apk/debug/app-debug.apk $PROJNAME-$VERSION-$BUILD_NUMBER.apk
#mv app/build/outputs/apk/debug/app-debug.apk ../rfid-docker/www/`date "+%Y-%m-%d--%H-%M"`.apk
